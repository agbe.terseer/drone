# Getting Started

### Reference Documentation for Drone Service
For further reference, please consider the following sections:

* Build instructions
* Run Instructions
* Test Instructions
###  Build instructions
```
* Database setup ---- either Postgress or H2 username and password should be spercified
* For Postgress DB, create a Databse name before building the app.
* run mvn clean install
* run java -jar /target/fleetmanagement-0.1.jar

```
###  Run instructions

```
* run java -jar /target/fleetmanagement-0.1.jar
```
The following guides illustrate how to use some features concretely:


###  Test instructions
Step by step process

```
NOTE: RECORDS HAVE BEEN PRELOADED ONCE YOU RUN THE APPLICATION.

1. Drones
2. Medications
3. Items added to Drone.

You can proceed to test

```

1. Register a drone   -> http://localhost:8090/drone-service/api/v1/drones 


>REGISTER DRONE REQUEST BODY
```
{
"model": "Middleweight",
"weight": "100",
"batteryCapacity" :100,
"state": "IDLE"
}
```
---------------------------------

2. Register a medication  -> http://localhost:8090/drone-service/api/v1/medications
>REGISTER MEDICATION REQUEST BODY
ContentType = {form-data}
```
itemName = Lu18
itemWeight = 18
files = {path to file}
itemCode = DGSD_18
```
 ---------------------------------
3. Load Medication to a Drone - > http://localhost:8090/drone-service/api/v1/drones/load

>LOAD DRONE REQUEST BODY
```
{
"drone": {
"id": "1",
"model": "Cruiserweight",
"weight": "10",
"batteryCapacity" :10,
"state": "LOADED"
},
"medicationItems": [
{
"id": 5,
"name": "Lu18",
"weight": 18,
"code": "DGSD_18",
"image": "MicrosoftTeams-image (2).png"
}

]}
```
 
4. Checking loaded medication items for a given drone -> http://localhost:8090/drone-service/api/v1/drones/items/1?droneState=LOADED
5. Checking available drones for loading   -> http://localhost:8090/drone-service/api/v1/drones/available-drones
6. Check drone battery level for a given drone   http://localhost:8090/drone-service/api/v1/drones/battery-level/{droneId}
7. Periodic task to check drons batter level
```
This have been configure in the application-dev.yml 
you can select any to test. *** I have set five sec ***

job:
  cron:
    fivesec: 0/5 * * * * *   # 5 sec
    oneam: 0 0 0 * * ?  # 1 am every day
```