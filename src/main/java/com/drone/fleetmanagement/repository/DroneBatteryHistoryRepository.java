package com.drone.fleetmanagement.repository;

import com.drone.fleetmanagement.entity.DroneBatteryHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DroneBatteryHistoryRepository extends JpaRepository<DroneBatteryHistory, Long> {
}

