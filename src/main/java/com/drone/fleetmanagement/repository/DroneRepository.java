package com.drone.fleetmanagement.repository;

import com.drone.fleetmanagement.entity.Drone;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface DroneRepository extends JpaRepository<Drone, Long> {
    @Query("select d from Drone d where d.model = :#{#model} and d.weight= :#{#weight}")
    Optional<Drone> findDroneDetail(@Param("model") String model, @Param("weight") Integer weight);

    @Query("select d from Drone d where d.droneState = 'IDLE'")
    Page<Drone> findAllByAvailableDrone(Pageable pageable);


}
