package com.drone.fleetmanagement.repository;

import com.drone.fleetmanagement.entity.Drone;
import com.drone.fleetmanagement.entity.DroneDelivery;
import com.drone.fleetmanagement.enums.DroneState;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


import java.util.List;
import java.util.Optional;

@Repository
public interface DroneDeliveryRepository extends JpaRepository<DroneDelivery, Long> {
    @Override
    Optional<DroneDelivery> findById(Long aLong);

    @Query("select d from DroneDelivery d where d.drone =:drone and d.status =:droneState")
    Optional<DroneDelivery> findByDroneAndStatus(Drone drone, DroneState droneState);

    @Query("select d from DroneDelivery d where d.drone =:drone and d.status =:droneState")
    List<DroneDelivery> findAllByDroneAndStatus(Drone drone, DroneState droneState);

    List<DroneDelivery> findAllByDrone(Drone drone);


}
