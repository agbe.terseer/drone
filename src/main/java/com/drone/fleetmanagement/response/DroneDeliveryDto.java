package com.drone.fleetmanagement.response;

import com.drone.fleetmanagement.enums.DroneState;
import lombok.Data;

import java.util.List;

@Data
public class DroneDeliveryDto {
    private DroneResponse droneRegResponse;
    private List<MedicationResponse> medicationResponse;
    private DroneState status;

}
