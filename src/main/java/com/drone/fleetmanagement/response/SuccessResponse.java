package com.drone.fleetmanagement.response;

import com.drone.fleetmanagement.constant.Constants;
public class SuccessResponse extends ResponseHelper {

        public SuccessResponse(String message, Object data) {
            super(true, message, data);
        }

        public SuccessResponse(Object data) {
            super(true, Constants.SUCCESS_MESSAGE, data);
        }
}
