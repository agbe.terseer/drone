package com.drone.fleetmanagement.response;

import com.drone.fleetmanagement.entity.Drone;
import lombok.Data;

import java.util.List;

@Data
public class DroneDeliveryResponse {
    private Drone drone;
    private List<MedicationResponse> medications;
}
