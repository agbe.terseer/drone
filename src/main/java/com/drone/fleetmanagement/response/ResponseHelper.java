package com.drone.fleetmanagement.response;

import lombok.Getter;

import java.util.Date;

@Getter
public class ResponseHelper {
    private final Date timeStamp = new Date();
    private final Boolean status;
    private final String message;
    private final Object data;

    public ResponseHelper(Boolean status, String message, Object data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }


}
