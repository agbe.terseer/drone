package com.drone.fleetmanagement.response;

import lombok.Data;


@Data
public class MedicationResponse {
    private Long id;
    private String name;
    private Integer weight;
    private String code;
    private String image;
}
