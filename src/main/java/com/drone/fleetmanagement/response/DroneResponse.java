package com.drone.fleetmanagement.response;

import com.drone.fleetmanagement.enums.DroneState;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;


import java.util.Date;
import java.util.List;


@Data
public class DroneResponse {
    private Long id;
    private String model;
    private String serialNumber;
    private Integer weight;
    @JsonProperty("battery_capacity")
    private Integer batteryCapacity;
    private DroneState state;
    private Date createdAt;
    private List<MedicationResponse> medications;
}
