package com.drone.fleetmanagement.controller;

import com.drone.fleetmanagement.request.MedicationDto;
import com.drone.fleetmanagement.service.MedicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.concurrent.CompletableFuture;

import static com.drone.fleetmanagement.constant.Constants.API_V1;

@RestController
@CrossOrigin
@Validated
@RequestMapping(API_V1+"/medications")
public class MedicationController {

    private final MedicationService medicationService;

    @Autowired
    public MedicationController(MedicationService medicationService) {
        this.medicationService = medicationService;
    }


    @PostMapping(value = "",
            headers = {"content-type=multipart/mixed",
            "content-type=multipart/form-data"})
    public CompletableFuture<ResponseEntity<?>> addMedication(
                                                              @RequestParam("itemName") String itemName,
                                                              @RequestParam("itemWeight") String itemWeight,
                                                              @RequestParam("itemCode") String itemCode,
                                                              @RequestParam("files") MultipartFile file){

        MedicationDto medication = new MedicationDto(itemName,Integer.parseInt(itemWeight),itemCode);
    return CompletableFuture.completedFuture(medicationService.registerMedication(medication,file));
    }

    @GetMapping(value = "/{id}")
    public CompletableFuture<ResponseEntity<?>> getMedication(@PathVariable Long id){
        return CompletableFuture.completedFuture(medicationService.getSingleMedication(id));
    }

}
