package com.drone.fleetmanagement.controller;

import static com.drone.fleetmanagement.constant.Constants.*;

import com.drone.fleetmanagement.enums.DroneState;
import com.drone.fleetmanagement.request.DroneRegistrationRequest;
import com.drone.fleetmanagement.request.LoadDroneRequest;
import com.drone.fleetmanagement.service.DroneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@CrossOrigin
@RequestMapping(API_V1+"/drones")
public class DroneController {

    private final DroneService droneService;

    @Autowired
    public DroneController(DroneService droneService) {
        this.droneService = droneService;
    }

    @PostMapping
    public ResponseEntity<?> registerDrone(@Valid @RequestBody DroneRegistrationRequest request){
        return droneService.registerDrone(request);
    }


    @GetMapping("/{id}")
    public ResponseEntity<?> getSingleDrone(@Valid @PathVariable Long id){
        return droneService.getSingleDrone(id);
    }

    @PostMapping("/load")
    public ResponseEntity<?> loadDrone(@Valid @RequestBody LoadDroneRequest request){
        return droneService.loadDrone(request);
    }

    @GetMapping("/items/{droneId}")
    public ResponseEntity<?> checkLoadedMedicationItems(@Valid @PathVariable Long droneId, @RequestParam(defaultValue="LOADED") DroneState droneState){
        return droneService.checkLoadedMedicationItems(droneId, droneState);
    }

    @GetMapping("/available-drones")
    public ResponseEntity<?> checkAvailableDrones(@RequestParam(defaultValue="0") int page,
                                                 @RequestParam(defaultValue="10") int size){
        return droneService.checkAvailableDrones(page,size);
    }

    @GetMapping("/battery-level/{droneId}")
    public ResponseEntity<?> checkBatteryLevel(@PathVariable Long droneId){
        return droneService.checkBatteryLevel(droneId);
    }


}
