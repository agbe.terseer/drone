package com.drone.fleetmanagement.constant;

public class Constants {
    public static final String SUCCESS_MESSAGE = "Successful";
    public static final String ERROR_MESSAGE = "Error";
    public static final String MAX_WEIGHT = "";
    public static final String DUPLICATE_KEY = "";
    public static final String DUPLICATE_RECORD = "Duplicate records";
    public static final String BATTERY_PERCENT = "Battery percent cannot be zero";
    public static final String REQUESTBODY_NOT_NULL = "Request body List cannot be null or Empty";
    public static final String BATTERY_NOT_NULL = "Battery cannot be null";

    public static final String WEIGHT_NOT_NULL = "Weight cannot be null";
    public static final String INVALID_MODEL = "Invalid Model";
    public static final String NOT_FOUND = "Record not found";

    public static final String LOADED_DRONE_NOT_FOUND = "No loaded drone found";

    public static final String DRONE_NOT_AVAILABLE = "Drone not available for loading";
    public static final String BATTERY_LEVEL = "Drone cannot be loaded due to low battery level";
    public static final String DRONE_OVERLOAD = "Drone is overloaded: maximum weight is 500kg";
    public static final String INVALID_NAME = "Only letters, numbers -, _ are allowed for NAME";
    public static final String INVALID_CODE = "Only Upper case letters, underscore and numbers are allowed for CODE";
    public static final String API_V1 = "/api/v1";

    public static final int MINIMUM_BATTERY_LEVEL = 25;

    public static final String AUDIT_BATTERY = "Battery level less that 25%";
    public static final String AUDIT_BATTERY_GREATER = "Battery level greater that 25%";
}
