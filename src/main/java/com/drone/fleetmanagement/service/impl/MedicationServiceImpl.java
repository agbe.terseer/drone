package com.drone.fleetmanagement.service.impl;

import static com.drone.fleetmanagement.constant.Constants.*;
import com.drone.fleetmanagement.entity.Medication;
import com.drone.fleetmanagement.exception.CustomException;
import com.drone.fleetmanagement.repository.MedicationRepository;
import com.drone.fleetmanagement.request.MedicationDto;
import com.drone.fleetmanagement.response.MedicationResponse;
import com.drone.fleetmanagement.response.SuccessResponse;
import com.drone.fleetmanagement.service.MedicationService;
import com.drone.fleetmanagement.util.FileUploadUtil;
import com.drone.fleetmanagement.util.StringUtil;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;
import java.util.Objects;
import java.util.Optional;

@Service
public class MedicationServiceImpl implements MedicationService {

    private final MedicationRepository medicationRepository;

    private final ModelMapper modelMapper;

    @Autowired
    public MedicationServiceImpl(MedicationRepository medicationRepository,ModelMapper modelMapper) {
        this.medicationRepository = medicationRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    public ResponseEntity<?> registerMedication(MedicationDto request, MultipartFile files) {
        try{
            // validate input
            validateInput(request);

            Medication medication = modelMapper.map(request, Medication.class);

            String fileName = StringUtils.cleanPath(Objects.requireNonNull(files.getOriginalFilename()));

            medication.setImage(fileName);  // assuming medication accepts one image
            medication.setCreatedAt(new Date());

            medication = medicationRepository.save(medication);

            String uploadDir = "images/" + request.getCode();

            FileUploadUtil.saveFile(uploadDir, fileName, files);

            return new ResponseEntity<>(new SuccessResponse(getMedicationResponse(medication)), HttpStatus.OK);
        }catch (Exception ex){
            throw new CustomException(ex.getMessage(), HttpStatus.EXPECTATION_FAILED);
        }
    }

    @Override
    public ResponseEntity<?> getSingleMedication(Long id) {
        try{
            return new ResponseEntity<>(new SuccessResponse(getMedicationResponse(getMedication(id))), HttpStatus.OK);
        }catch (Exception ex){
            throw new CustomException(ex.getMessage(), HttpStatus.EXPECTATION_FAILED);
        }
    }

    public Medication getMedication(Long id){
        Optional<Medication> medication = medicationRepository.findById(id);
        if (medication.isEmpty())
            throw new CustomException(NOT_FOUND, HttpStatus.NOT_FOUND);
        return medication.get();
    }


    private MedicationResponse getMedicationResponse(Medication medication){
        return modelMapper.map(medication, MedicationResponse.class);
    }

    private void validateInput(MedicationDto request){
        if (!StringUtil.isAlphaNumeric(request.getName()))
            throw new CustomException(INVALID_NAME, HttpStatus.EXPECTATION_FAILED);

        if (!StringUtil.isValidateCode(request.getCode()))
            throw new CustomException(INVALID_CODE, HttpStatus.EXPECTATION_FAILED);

    }

}
