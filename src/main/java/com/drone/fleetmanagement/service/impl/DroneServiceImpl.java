package com.drone.fleetmanagement.service.impl;

import static com.drone.fleetmanagement.constant.Constants.*;
import com.drone.fleetmanagement.entity.Drone;
import com.drone.fleetmanagement.entity.DroneDelivery;
import com.drone.fleetmanagement.entity.Medication;
import com.drone.fleetmanagement.enums.DroneState;
import com.drone.fleetmanagement.enums.ModelType;
import com.drone.fleetmanagement.exception.CustomException;
import com.drone.fleetmanagement.mapper.MedicationItemsMapper;
import com.drone.fleetmanagement.repository.DroneDeliveryRepository;
import com.drone.fleetmanagement.repository.DroneRepository;
import com.drone.fleetmanagement.request.DronePojo;
import com.drone.fleetmanagement.request.DroneRegistrationRequest;
import com.drone.fleetmanagement.request.LoadDroneRequest;
import com.drone.fleetmanagement.request.MedicationRequest;
import com.drone.fleetmanagement.response.*;
import com.drone.fleetmanagement.service.DroneService;
import com.drone.fleetmanagement.service.MedicationService;
import com.drone.fleetmanagement.util.StringUtil;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.text.SimpleDateFormat;
import java.util.*;


@Service
public class DroneServiceImpl implements DroneService {

    private final DroneRepository droneRepository;
    private final DroneDeliveryRepository droneDeliveryRepository;
    private final ModelMapper modelMapper;

    private final MedicationService medicationService;

    @Autowired
    public DroneServiceImpl(DroneRepository droneRepository, DroneDeliveryRepository droneDeliveryRepository, ModelMapper modelMapper, MedicationService medicationService) {
        this.droneRepository = droneRepository;
        this.droneDeliveryRepository = droneDeliveryRepository;
        this.modelMapper = modelMapper;
        this.medicationService = medicationService;
    }


    @Override
    public ResponseEntity<?> registerDrone(DroneRegistrationRequest request) {

        try{
            if (request == null)
                throw new CustomException(REQUESTBODY_NOT_NULL,HttpStatus.NOT_EXTENDED);

            validation(request);

            Drone drone = new Drone();
            drone.setBatteryCapacity(request.getBatteryCapacity());
            drone.setModel(request.getModel());
            drone.setSerialNumber(getSerialNumber(request));
            drone.setDroneState(request.getState());
            drone.setWeight(request.getWeight());
            drone.setCreatedAt(new Date());

            return new ResponseEntity<>(new SuccessResponse(getDroneResponse(droneRepository.save(drone))), HttpStatus.OK);
        }catch (Exception ex){
            throw new CustomException(ex.getMessage(), HttpStatus.EXPECTATION_FAILED);
        }

    }


    public ResponseEntity<?> getSingleDrone(Long id){
        return new ResponseEntity<>(new SuccessResponse(getDroneResponse(getDrone(id))), HttpStatus.OK);
    }

    private DroneResponse getDroneResponse(Drone drone){
        DroneResponse droneRegResponse = new DroneResponse();
        droneRegResponse.setId(drone.getId());
        droneRegResponse.setModel(drone.getModel());
        droneRegResponse.setState(drone.getDroneState());
        droneRegResponse.setWeight(drone.getWeight());
        droneRegResponse.setBatteryCapacity(drone.getBatteryCapacity());
        droneRegResponse.setSerialNumber(drone.getSerialNumber());
        droneRegResponse.setCreatedAt(drone.getCreatedAt());
        return droneRegResponse;
    }

    private String getSerialNumber(DroneRegistrationRequest request){
        String initials = request.getModel();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy");
        dateFormat.format(new Date());
        return StringUtil.generateNumber(initials);
    }

    private void validation(DroneRegistrationRequest request) {

        validateModel(request.getModel());

        if (request.getBatteryCapacity() == null) {
            throw new CustomException(BATTERY_NOT_NULL, HttpStatus.EXPECTATION_FAILED);
        }

        if (request.getWeight() == null) {
            throw new CustomException(WEIGHT_NOT_NULL, HttpStatus.EXPECTATION_FAILED);
        }

        Optional<Drone> droneOptional = droneRepository.findDroneDetail(request.getModel(), request.getWeight());
        if (droneOptional.isPresent())
            throw new CustomException(DUPLICATE_RECORD, HttpStatus.EXPECTATION_FAILED);
    }

    /**
     * This method checks for valid weight during drone registration
     */
    public void validateModel(String model){
        final List<String> models= Arrays.asList(ModelType.CRUISER_WEIGHT.getValue(),ModelType.HEAVY_WEIGHT.getValue(),
                ModelType.MIDDLE_WEIGHT.getValue(),ModelType.LIGHT_WEIGHT.getValue());

        if(List.of(models).contains(model)){
            throw new CustomException(INVALID_MODEL,HttpStatus.EXPECTATION_FAILED);
        }
    }

    // ####################### Loading a drone with medication items  #############

    @Transactional
    @Override
    public ResponseEntity<?> loadDrone(LoadDroneRequest request) {

        Long droneID = request.getDrone().getId();

        checkBatteryLevelDuringDroneLoading(droneID);

        Drone drone = getDrone(droneID);

        if (!isDroneAvailableForLoading(drone.getId()))
            throw new CustomException(DRONE_NOT_AVAILABLE, HttpStatus.EXPECTATION_FAILED);


        int totalWeight = calculateTotalWeight(request.getMedicationItems(), request.getDrone());

        DroneDeliveryResponse droneDeliveryDto = addMoreITerms(drone, request.getMedicationItems());

        updateDroneStatus(totalWeight,drone);

        return new ResponseEntity<>(new SuccessResponse(droneDeliveryDto), HttpStatus.OK);

    }

    private DroneDeliveryResponse addMoreITerms(Drone drone, List<MedicationRequest> requests){
        for (MedicationRequest request : requests) {
            DroneDelivery delivery = new DroneDelivery();
            delivery.setDrone(drone);
            delivery.setMedication(medicationService.getMedication(request.getId()));
            delivery.setStatus(DroneState.LOADING);
            droneDeliveryRepository.save(delivery);
        }
        return getDroneDeliveryResponse(drone);
    }


    private DroneDeliveryResponse getDroneDeliveryResponse(Drone drone){
        DroneDeliveryResponse delivery = new DroneDeliveryResponse();
        delivery.setDrone(drone);
        delivery.setMedications(getMedicationResponse(drone));
        return delivery;
    }

    private List<MedicationResponse> getMedicationResponse(Drone drone){
        try{
            ArrayList<Medication> medications = new ArrayList<>();
            List<DroneDelivery> list = droneDeliveryRepository.findAllByDroneAndStatus(drone, drone.getDroneState());
            for (DroneDelivery delivery: list){
                medications.add(delivery.getMedication());
            }
            return MedicationItemsMapper.mapToResponseList(medications, modelMapper);
        }catch (Exception ex){
            throw new CustomException(ex.getMessage(), HttpStatus.EXPECTATION_FAILED);
        }

    }


    /**
     * get drone by Loaded status --->> and display the loaded items
     *
     */
    @Override
    public ResponseEntity<?> checkLoadedMedicationItems(Long drone, DroneState droneState) {
        try {
            Drone drone1 = getDrone(drone);
            if (!drone1.getDroneState().equals(droneState))
                throw new CustomException(NOT_FOUND, HttpStatus.NOT_FOUND);

           return new ResponseEntity<>(new SuccessResponse(getDroneDeliveryResponse(getDrone(drone))), HttpStatus.OK);
        }catch (Exception ex){
            throw new CustomException(ex.getMessage(), HttpStatus.EXPECTATION_FAILED);
        }

    }

    @Override
    public ResponseEntity<?> checkAvailableDrones(int page, int size) {
        try {

            Pageable pageable = PageRequest.of(page,size);

            Page<Drone> dronePage = droneRepository.findAllByAvailableDrone(pageable);
            List<Drone> droneList = dronePage.getContent();

            if (droneList.isEmpty())
                throw new CustomException(LOADED_DRONE_NOT_FOUND, HttpStatus.NOT_FOUND);

            Map<String, Object> map = new HashMap<>();
            map.put("drones", droneList);
            map.put("currentPages", dronePage.getNumber());
            map.put("totalItems", dronePage.getTotalElements());
            map.put("totalIPages", dronePage.getTotalPages());

            return new ResponseEntity<>(new SuccessResponse(map), HttpStatus.OK);
        }catch (Exception ex){
            throw new CustomException(ex.getMessage(), HttpStatus.EXPECTATION_FAILED);
        }
    }

    @Override
    public ResponseEntity<?> checkBatteryLevel(Long droneId) {
        try{

            Optional<Drone> drone = droneRepository.findById(droneId);
            if (drone.isEmpty())
                throw new CustomException(NOT_FOUND, HttpStatus.NOT_FOUND);

            Map<String, Object> map = new HashMap<>();
            map.put("batteryLevel", drone.get().getBatteryCapacity());

            return new ResponseEntity<>(new SuccessResponse(map), HttpStatus.OK);
        }catch (Exception ex){
            throw new CustomException(ex.getMessage(), HttpStatus.EXPECTATION_FAILED);
        }
    }


    /**
     * This method checks the total weight of the Items being added to the drone
     * Prevents the drone from being loaded with more weight than it can carry
     */
    private int calculateTotalWeight(List<MedicationRequest> weights, DronePojo drone){

        Drone drone1 = getDrone(drone.getId());
        int totalRequestWeight = 0;
        for (MedicationRequest data: weights){
            totalRequestWeight += data.getWeight();
        }

        int totalWeightInRecord = calculateAccumulateWeight(drone);

        int totalWeight = totalRequestWeight + totalWeightInRecord;

        if (totalWeight > drone1.getWeight())
            throw new CustomException(DRONE_OVERLOAD, HttpStatus.EXPECTATION_FAILED);

        return totalWeight;
    }

    private boolean isDroneAvailableForLoading(Long droneId){
        Drone drone = getDrone(droneId);
        return drone.getDroneState().equals(DroneState.IDLE) || drone.getDroneState().equals(DroneState.LOADING);
    }

    private void updateDroneStatus(int totalWeight, Drone drone1){

        try{
            DroneState newDroneState;

           if (drone1.getWeight() == totalWeight)
               newDroneState = DroneState.LOADED;
           else if (totalWeight < drone1.getWeight())
               newDroneState = DroneState.LOADING;
           else
               newDroneState = DroneState.IDLE;

           if (newDroneState.equals(DroneState.LOADED)){
               updateDeliveryStatus(drone1.getDroneState().name(), newDroneState.name(), drone1);
           }else {
               updateDeliveryStatus(newDroneState.name(), drone1);
           }

        }catch (Exception ex){
            throw new CustomException(ex.getMessage(),HttpStatus.NOT_FOUND);
        }
    }

    private void updateDeliveryStatus(String droneState, Drone drone1){

        try{
            List<DroneDelivery> delivery = droneDeliveryRepository.findAllByDroneAndStatus(drone1,DroneState.valueOf(droneState));
            if (delivery.isEmpty())
                throw new CustomException(NOT_FOUND, HttpStatus.NOT_FOUND);

            for (DroneDelivery data: delivery){
                data.setStatus(DroneState.valueOf(droneState));
                droneDeliveryRepository.save(data);
            }

            drone1.setDroneState(DroneState.valueOf(droneState));
            droneRepository.save(drone1);
        }catch (Exception ex){
           throw new CustomException(ex.getMessage(), HttpStatus.EXPECTATION_FAILED);
        }
    }

    private void updateDeliveryStatus(String droneStateOld, String droneStateNew, Drone drone1){

        try{
            List<DroneDelivery> delivery = droneDeliveryRepository.findAllByDroneAndStatus(drone1,DroneState.valueOf(droneStateOld));
            if (delivery.size() == 0)
                throw new CustomException(NOT_FOUND, HttpStatus.NOT_FOUND);

            for (DroneDelivery data: delivery){
                data.setStatus(DroneState.valueOf(droneStateNew));
                droneDeliveryRepository.save(data);
            }
            drone1.setDroneState(DroneState.valueOf(droneStateNew));
            droneRepository.save(drone1);
        }catch (Exception ex){
            throw new CustomException(ex.getMessage(), HttpStatus.EXPECTATION_FAILED);
        }
    }



    /**
     * This method check if the drone has more space to carry additional items
     * @param drone
     * &#064;Description
     */
    private int calculateAccumulateWeight(DronePojo drone){
        Drone drone1 = getDrone(drone.getId());
        List<DroneDelivery> droneDeliveryList = droneDeliveryRepository.findAllByDroneAndStatus(drone1,DroneState.valueOf(DroneState.LOADING.name()));
        int totalWeight = 0;

        if(droneDeliveryList.size() > 0){
            for (DroneDelivery droneDelivery : droneDeliveryList) {
                totalWeight += droneDelivery.getMedication().getWeight();
            }
        }
        return totalWeight;
    }

    private Drone getDrone(Long id){
        try{
            Optional<Drone> drone = droneRepository.findById(id);
            if (drone.isEmpty())
                throw new CustomException(NOT_FOUND, HttpStatus.NOT_FOUND);
            return drone.get();
        }catch (Exception ex){
            throw new CustomException(ex.getMessage(), HttpStatus.EXPECTATION_FAILED);
        }
    }

    private void checkBatteryLevelDuringDroneLoading(Long droneId){
        Drone drone1 = getDrone(droneId);
        if (drone1.getBatteryCapacity() < MINIMUM_BATTERY_LEVEL)
            throw new CustomException(BATTERY_LEVEL, HttpStatus.EXPECTATION_FAILED);
    }


}
