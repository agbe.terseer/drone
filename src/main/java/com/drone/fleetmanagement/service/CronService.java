package com.drone.fleetmanagement.service;

import com.drone.fleetmanagement.entity.Drone;
import com.drone.fleetmanagement.entity.DroneBatteryHistory;
import com.drone.fleetmanagement.exception.CustomException;
import com.drone.fleetmanagement.repository.DroneBatteryHistoryRepository;
import com.drone.fleetmanagement.repository.DroneRepository;
import com.drone.fleetmanagement.request.BatteryHistoryRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

import static com.drone.fleetmanagement.constant.Constants.*;

@Slf4j
@Service
public class CronService {


    private final DroneBatteryHistoryRepository droneBatteryHistoryRepository;
    private final DroneRepository droneRepository;


    @Autowired
    public CronService(DroneBatteryHistoryRepository droneBatteryHistoryRepository, DroneRepository droneRepository) {
        this.droneBatteryHistoryRepository = droneBatteryHistoryRepository;
        this.droneRepository = droneRepository;

    }


    @Scheduled(cron = "${job.cron.fivesec}")
    public void droneBatteryPeriodicCheck(){
        log.info(" +++++++++++++++++++++++ running periodic checks on drone battery +++++++++++++++++");

        List<Drone> droneList = droneRepository.findAll();
        for (Drone data: droneList){
            checkBatteryLevelDuringDroneLoading(data);
        }
        log.info(" +++++++++++++++++++++++ end periodic checks on drone battery +++++++++++++++++");

    }

    public void checkBatteryLevelDuringDroneLoading(Drone drone){
        BatteryHistoryRequest data;
        if (drone.getBatteryCapacity() < MINIMUM_BATTERY_LEVEL){
            data = new BatteryHistoryRequest(0L, drone, drone.getBatteryCapacity(), AUDIT_BATTERY);
        }else{
            data = new BatteryHistoryRequest(0L, drone, drone.getBatteryCapacity(), AUDIT_BATTERY_GREATER);
        }
        createBatteryLogs(data);
    }


    private void createBatteryLogs(BatteryHistoryRequest request){
        try{
            DroneBatteryHistory droneBatteryHistory = new DroneBatteryHistory();
            droneBatteryHistory.setDrone(request.getDrone());
            droneBatteryHistory.setBatteryCapacity(request.getBatteryCapacity());
            droneBatteryHistory.setComment(request.getComment());
            droneBatteryHistory.setCreatedAt(new Date());
            droneBatteryHistoryRepository.save(droneBatteryHistory);
        }catch (Exception ex){
            throw new CustomException(ex.getMessage(), HttpStatus.EXPECTATION_FAILED);
        }

    }

}
