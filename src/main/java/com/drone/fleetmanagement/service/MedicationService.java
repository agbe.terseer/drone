package com.drone.fleetmanagement.service;

import com.drone.fleetmanagement.entity.Medication;
import com.drone.fleetmanagement.request.MedicationDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

public interface MedicationService {
    ResponseEntity<?> registerMedication(MedicationDto request, MultipartFile files);

    ResponseEntity<?> getSingleMedication(Long id);

    Medication getMedication(Long id);
}
