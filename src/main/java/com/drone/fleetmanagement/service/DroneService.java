package com.drone.fleetmanagement.service;

import com.drone.fleetmanagement.enums.DroneState;
import com.drone.fleetmanagement.request.DroneRegistrationRequest;
import com.drone.fleetmanagement.request.LoadDroneRequest;
import org.springframework.http.ResponseEntity;

public interface DroneService {

    ResponseEntity<?> registerDrone(DroneRegistrationRequest request);

    ResponseEntity<?> getSingleDrone(Long id);

    ResponseEntity<?> loadDrone(LoadDroneRequest request);

    ResponseEntity<?> checkLoadedMedicationItems(Long drone, DroneState request);

    ResponseEntity<?> checkAvailableDrones(int page, int size);

    ResponseEntity<?> checkBatteryLevel(Long droneId);


}
