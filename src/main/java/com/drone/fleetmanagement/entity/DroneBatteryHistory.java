package com.drone.fleetmanagement.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class DroneBatteryHistory extends SuperModel {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;



    @ManyToOne
    private Drone drone;

    @Column(length = 100)
    @JsonProperty("battery_capacity")
    private Integer batteryCapacity;

    private String comment;

}
