package com.drone.fleetmanagement.entity;

import com.drone.fleetmanagement.enums.DroneState;
import lombok.Data;
import javax.persistence.*;

@Data
@Entity
public class DroneDelivery {


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "drone_delivery_id_seq")
    @SequenceGenerator(name = "drone_delivery_id_seq", sequenceName = "drone_delivery_id_seq", allocationSize = 1)
    private Long id;

    @ManyToOne
    private Drone drone;

    @ManyToOne
    private Medication medication;

    @Enumerated(EnumType.STRING)
    private DroneState status;
}
