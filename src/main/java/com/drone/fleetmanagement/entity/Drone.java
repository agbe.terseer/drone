package com.drone.fleetmanagement.entity;


import com.drone.fleetmanagement.enums.DroneState;
import com.fasterxml.jackson.annotation.JsonProperty;
import javax.validation.constraints.NotNull;
import lombok.Data;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Data
@Entity
public class Drone extends SuperModel{

    private static final long serialVersionUID = -1831508458034406573L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "drone_id_seq")
    @SequenceGenerator(name = "drone_id_seq", sequenceName = "drone_id_seq", allocationSize = 1)
    private Long id;

    @NotNull
    @Column(length = 100, unique = true)
    @JsonProperty("serial_number")
    private String serialNumber; //serial number (100 characters max);

    private String model; //(Lightweight, Middleweight, Cruiserweight, Heavyweight);

    @NotNull
    @Column(length = 500)
    private Integer weight; //limit (500gr max);

    @Column(length = 100)
    private Integer batteryCapacity; //assuming percentage is stored as Integer //battery capacity (percentage);

    @Enumerated(EnumType.STRING)
    @NotNull
    private DroneState droneState; //state (IDLE, LOADING, LOADED, DELIVERING, DELIVERED, RETURNING)


//    @JsonIgnore
//    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
//    private List<Medication> medication;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Drone)) return false;
        Drone drone = (Drone) o;
        return Objects.equals(getId(), drone.getId()) && getSerialNumber().equals(drone.getSerialNumber()) && getModel().equals(drone.getModel()) && getWeight().equals(drone.getWeight()) && getBatteryCapacity().equals(drone.getBatteryCapacity()) && getDroneState() == drone.getDroneState();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getSerialNumber(), getModel(), getWeight(), getBatteryCapacity(), getDroneState());
    }
}
