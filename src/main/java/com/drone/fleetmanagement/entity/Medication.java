package com.drone.fleetmanagement.entity;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.util.Objects;

@Data
@Entity
public class Medication extends SuperModel{
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "medication_id_seq")
    @SequenceGenerator(name = "medication_id_seq", sequenceName = "medication_id_seq", allocationSize = 1)
    private Long id;

    private String name; //name (allowed only letters, numbers, ‘-‘, ‘_’);

    @Min(1)
    @Max(100)
    private Integer weight;     // Assuming the maximum weight to be 50kg.

    private String code; //code (allowed only upper case letters, underscore and numbers);
    private String image; //image (picture of the medication case).

//    @JsonIgnore
//    @ManyToOne
//    private Drone drone;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Medication)) return false;
        Medication that = (Medication) o;
        return Objects.equals(getId(), that.getId()) && getName().equals(that.getName()) && getWeight().equals(that.getWeight()) && getCode().equals(that.getCode()) && Objects.equals(getImage(), that.getImage());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName(), getWeight(), getCode(), getImage());
    }
}
