package com.drone.fleetmanagement.mapper;

import com.drone.fleetmanagement.entity.Medication;
import com.drone.fleetmanagement.request.MedicationRequest;
import com.drone.fleetmanagement.response.MedicationResponse;
import org.modelmapper.ModelMapper;

import java.util.List;
import java.util.stream.Collectors;

public class MedicationItemsMapper {
    public static MedicationRequest mapToRequest(Medication entity, ModelMapper modelMapper){
        if(entity == null){
            return null;
        }
        return modelMapper.map(entity, MedicationRequest.class);
    }

    public static MedicationResponse mapToResponse(Medication entity, ModelMapper modelMapper){
        if(entity == null){
            return null;
        }
        return modelMapper.map(entity, MedicationResponse.class);
    }

    public static List<MedicationRequest> mapToRequestList(List<Medication> entityList, ModelMapper modelMapper){
        return entityList.stream().map(s -> mapToRequest(s, modelMapper)).collect(Collectors.toList());
    }
    public static List<MedicationResponse> mapToResponseList(List<Medication> entityList, ModelMapper modelMapper){
        return entityList.stream().map(s -> mapToResponse(s, modelMapper)).collect(Collectors.toList());
    }
}
