package com.drone.fleetmanagement.request;

import com.drone.fleetmanagement.enums.DroneState;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DronePojo {
    private Long id;

    @NotBlank(message = "Model is required")
    private String model;

    @Min(1)
    @Max(500)
    private Integer weight;

    @Min(1)
    @Max(100)
    private Integer batteryCapacity; //assuming percentage is stored as Integer //battery capacity (percentage);

    private DroneState state = DroneState.IDLE; //state (IDLE, LOADING, LOADED, DELIVERING, DELIVERED, RETURNING)

}
