package com.drone.fleetmanagement.request;

import com.drone.fleetmanagement.entity.Drone;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;


@Data
@AllArgsConstructor
public class BatteryHistoryRequest {

    private Long id;
    private Drone drone;
    @JsonProperty("battery_capacity")
    private Integer batteryCapacity;
    private String comment;
}
