package com.drone.fleetmanagement.request;

import com.drone.fleetmanagement.util.ValidName;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class MedicationRequest {

    private Long id;

    @ValidName
    private String name;

    @NotBlank(message = "weight is required")
    private Integer weight;

    @NotBlank
    private String code;
}
