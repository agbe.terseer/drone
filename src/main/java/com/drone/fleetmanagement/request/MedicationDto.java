package com.drone.fleetmanagement.request;

import com.drone.fleetmanagement.util.ValidName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

@Data
@NoArgsConstructor
public class MedicationDto {
    @ValidName
    private String name;

    @Min(1)
    @Max(50)
    private Integer weight;

    @NotBlank
    private String code;

    public MedicationDto(String name, Integer weight, String code) {
        this.name = name;
        this.weight = weight;
        this.code = code;
    }
}
