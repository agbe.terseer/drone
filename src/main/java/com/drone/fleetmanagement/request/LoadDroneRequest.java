package com.drone.fleetmanagement.request;

import lombok.Data;

import java.util.List;

@Data
public class LoadDroneRequest {
    private List<MedicationRequest> medicationItems;
    private DronePojo drone;

}
