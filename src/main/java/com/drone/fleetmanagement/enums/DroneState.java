package com.drone.fleetmanagement.enums;

import com.drone.fleetmanagement.util.CommonUtils;

import java.util.Optional;

public enum DroneState {
    IDLE, LOADING, LOADED, DELIVERING, DELIVERED, RETURNING;
    public static Optional<DroneState> find(String value){
        if (CommonUtils.isNonEmpty(value)){
            try {
                return Optional.of(DroneState.valueOf(value.toUpperCase()));
            } catch (IllegalArgumentException e) {
                return Optional.empty();
            }
        }
        return Optional.empty();
    }

}
