package com.drone.fleetmanagement.enums;

public enum ModelType {
    LIGHT_WEIGHT("Lightweight"), MIDDLE_WEIGHT("middleweight"),
    CRUISER_WEIGHT("Cruiserweight"),HEAVY_WEIGHT("Heavyweight");

    private String value;

    ModelType(String value){
        this.value = value;
    }
    public String getValue(){
        return value;
    }
}
