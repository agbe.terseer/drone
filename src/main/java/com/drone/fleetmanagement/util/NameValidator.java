package com.drone.fleetmanagement.util;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;


public class NameValidator implements ConstraintValidator<ValidName,String> {

    @Override
    public void initialize(ValidName constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(String nameFiled, ConstraintValidatorContext constraintValidatorContext) {
        if(nameFiled == null)
            return true;

        boolean val = StringUtil.isAlphaNumeric(nameFiled);
        System.out.println(" VAL = "+ val);
        return val;
    }
}
