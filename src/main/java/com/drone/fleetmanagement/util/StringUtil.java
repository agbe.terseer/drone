package com.drone.fleetmanagement.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicInteger;


public class StringUtil {

    public static String generateNumber(String data) {
        String pattern = "yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

        String date = simpleDateFormat.format(new Date());
        System.out.println(date);

        Long currentDateTime = System.currentTimeMillis();
        String cusNumber = String.valueOf(currentDateTime);

        return data.substring(0,4) + "-" +cusNumber.substring(9).toUpperCase() + "-" + date;
    }

    // a Map for holding the sequencing
    private ConcurrentMap<String, AtomicInteger> _sequence =
            new ConcurrentHashMap<>();

    /**
     * Returns a unique, incrementing sequence, formatted to
     * 0 prefixed, 3 places, based upon the Drone model
     * and the registration year
     */
    public String getSequence(String initials, String year) {
        String key = makePrefix(initials, year);
        AtomicInteger chk = new AtomicInteger(0);
        AtomicInteger ai = _sequence.putIfAbsent(key, chk);
        if (ai == null) {
            ai = chk;
        }

        int val = ai.incrementAndGet();

        return String.format("%03d", val);
    }

    /**
     * A helper method to make the prefix, which is the
     * concatination of the initials, a "-", and a year.
     */
    private String makePrefix(String initials, String year) {
        return initials + "-" + year;
    }

    public static boolean isNumeric(String value){
        return value.matches("^[0-9]*$");
    }

    public static boolean isAlphaNumeric(String value){
        return value.matches("^[a-zA-Z0-9_.-]*$");
    }

    //Upper case letters, underscore and numbers are allowed"
    public static boolean isValidateCode(String value){
        return value.matches("^[A-Z0-9_.-]*$");  // ^[A-Z]+(?:_[A-Z]+)*$
    }


}
