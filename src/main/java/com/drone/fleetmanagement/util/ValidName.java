package com.drone.fleetmanagement.util;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Target({ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = NameValidator.class)
public @interface ValidName {
    String message() default "Only letters, numbers, - _ are allowed";
    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
