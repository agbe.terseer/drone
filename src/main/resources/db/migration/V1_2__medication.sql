DROP SEQUENCE IF EXISTS medication_id_seq;
CREATE SEQUENCE medication_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE IF NOT EXISTS public.medication
(
    id bigint DEFAULT nextval('medication_id_seq') NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    code character varying(255) COLLATE pg_catalog."default",
    image character varying(255) COLLATE pg_catalog."default",
    name character varying(255) COLLATE pg_catalog."default",
    weight integer,
    CONSTRAINT medication_pkey PRIMARY KEY (id),
    CONSTRAINT medication_weight_check CHECK (weight <= 100 AND weight >= 1)
    );

INSERT INTO public.medication(id, created_at, code, image, name, weight) VALUES
(1, '2022-09-10 14:47:55.223', 'DGSD_01', 'MicrosoftTeams-image (2).png', 'Lu01', 25),
(2, '2022-09-10 14:47:55.223', 'DGSD_02', 'MicrosoftTeams-image (2).png', 'Lu02', 25),
(3, '2022-09-10 14:47:55.223', 'DGSD_03', 'MicrosoftTeams-image (2).png', 'Lu03', 25),
(4, '2022-09-10 14:47:55.223', 'DGSD_04', 'MicrosoftTeams-image (2).png', 'Lu04', 25),
(5, '2022-09-10 14:47:55.223', 'DGSD_05', 'MicrosoftTeams-image (2).png', 'Lu05', 5);

SELECT setval('medication_id_seq', 5);