
DROP SEQUENCE IF EXISTS drone_id_seq;
CREATE SEQUENCE drone_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE IF NOT EXISTS public.drone
(
    id bigint DEFAULT nextval('drone_id_seq') NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    battery_capacity integer,
    drone_state character varying(255) COLLATE pg_catalog."default" NOT NULL,
    model character varying(255) COLLATE pg_catalog."default",
    serial_number character varying(100) COLLATE pg_catalog."default" NOT NULL,
    weight integer NOT NULL,
    CONSTRAINT drone_pkey PRIMARY KEY (id),
    CONSTRAINT uk_dec40dj9sujacyy3n8t5l80ry UNIQUE (serial_number)
    );

INSERT INTO drone ("id", "serial_number", "model", "weight", "battery_capacity", "drone_state", "created_at") VALUES
(1,'Crui-0252-2022', 'Cruiserweight', '50', '19', 'IDLE', '2022-09-10 14:47:55.223'),
(2,'Ligh-5220-2022', 'Lightweight', '159', '10', 'IDLE', '2022-09-10 14:47:55.223'),
(3,'Heav-6172-2022', 'Heavyweight', '200', '93', 'IDLE', '2022-09-10 14:49:56.176'),
(4,'Midd-5514-2022', 'Middleweight', '100', '56', 'IDLE', '2022-09-10 14:51:45.517');

SELECT setval('drone_id_seq', 5);