DROP SEQUENCE IF EXISTS drone_delivery_id_seq;
CREATE SEQUENCE drone_delivery_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE IF NOT EXISTS public.drone_delivery
(
    id bigint DEFAULT nextval('drone_delivery_id_seq') NOT NULL,
    status character varying(255) COLLATE pg_catalog."default",
    drone_id bigint,
    medication_id bigint,
    CONSTRAINT drone_delivery_pkey PRIMARY KEY (id),
    CONSTRAINT fk3ynrbvsqjspmb49hikyjrt24b FOREIGN KEY (medication_id)
    REFERENCES public.medication (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION,
    CONSTRAINT fke9rde1jmu7wbn4rufhf8211jh FOREIGN KEY (drone_id)
    REFERENCES public.drone (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION
    );

INSERT INTO public.drone_delivery( id, status, drone_id, medication_id) VALUES
(1, 'LOADED', 1, 1),
(2, 'LOADED', 1, 2),
(3, 'LOADED', 1, 3),
(4, 'LOADED', 1, 4);

SELECT setval('drone_delivery_id_seq', 5);