package com.drone.fleetmanagement.service.impl;

import com.drone.fleetmanagement.entity.Medication;
import com.drone.fleetmanagement.repository.MedicationRepository;
import com.drone.fleetmanagement.request.MedicationDto;
import com.drone.fleetmanagement.service.MedicationService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ActiveProfiles;

import java.nio.charset.StandardCharsets;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Slf4j
@ActiveProfiles("test")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@SpringBootTest
class MedicationServiceImplTest {

    @Autowired
    private MedicationRepository medicationRepository;

    @Autowired
    private MedicationService medicationService;

    final MockMultipartFile file = new MockMultipartFile("file", "snapshot.png", MediaType.IMAGE_JPEG_VALUE, "content".getBytes(StandardCharsets.UTF_8));

    public MedicationDto medicationDto = null;
    public Medication medication = null;
    public Medication medication2 = null;
    @BeforeAll
    void setUp() {
        medicationDto = new MedicationDto();
        medicationDto.setCode("DGSDAU_18");
        medicationDto.setName("Luau18");
        medicationDto.setWeight(40);

        medication = new Medication();
        medication.setCreatedAt(new Date());
        medication.setImage("snapshot.png");
        medication.setId(2L);
        medication.setCode("DGSDAU_19");
        medication.setName("Luau19 ");
        medication.setWeight(40);

        medication2 = medicationRepository.save(medication);

    }


    @Test
    void registerMedication() {

        ResponseEntity<?> responseHelperResponseEntity = medicationService.registerMedication(medicationDto,file);
        assertEquals(HttpStatus.OK, responseHelperResponseEntity.getStatusCode());

    }

    @Test
    void getSingleMedication() {
        ResponseEntity<?> responseHelperResponseEntity = medicationService.getSingleMedication(medication2.getId());
        assertEquals(HttpStatus.OK, responseHelperResponseEntity.getStatusCode());
    }

    @Test
    void getMedication() {
       Medication medication22 = medicationService.getMedication(medication2.getId());
       assertEquals(medication2, medication22);
    }
}