package com.drone.fleetmanagement.service.impl;


import com.drone.fleetmanagement.entity.Drone;
import com.drone.fleetmanagement.entity.Medication;
import com.drone.fleetmanagement.enums.DroneState;
import com.drone.fleetmanagement.enums.ModelType;
import com.drone.fleetmanagement.exception.CustomException;
import com.drone.fleetmanagement.repository.DroneRepository;
import com.drone.fleetmanagement.repository.MedicationRepository;
import com.drone.fleetmanagement.request.*;
import com.drone.fleetmanagement.response.DroneDeliveryResponse;
import com.drone.fleetmanagement.service.DroneService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@Slf4j
@ActiveProfiles("test")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@SpringBootTest
class DroneServiceImplTest {

    @Autowired
    private DroneService droneService;

    @Autowired
    private DroneRepository droneRepository;

    @Autowired
    private MedicationRepository medicationRepository;


    public DroneDeliveryResponse droneDeliveryDto = null;
    public Drone drone = null;
    public Medication medication = null;
    public LoadDroneRequest loadDroneRequest = null;
    public MedicationRequest medicationRequest = null;
    public DronePojo dronePojo = null;

    public MedicationDto medicationDto = null;

    @BeforeAll
    void setUp() {

        drone = new Drone();
        drone.setId(1L);
        drone.setDroneState(DroneState.IDLE);
        drone.setModel("Middleweight");
        drone.setWeight(100);
        drone.setSerialNumber("Ligh-5220-2022");
        drone.setBatteryCapacity(100);
        droneRepository.save(drone);

        dronePojo = new DronePojo( drone.getId(), drone.getModel(), drone.getWeight(), drone.getBatteryCapacity(), drone.getDroneState());


        medication = new Medication();
        medication.setImage("MicrosoftTeams-image (2).png");
        medication.setWeight(10);
        medication.setId(1L);
        medication.setCode("DGSD_18");
        medication.setName("Lu18");
        medication.setCreatedAt(new Date());
        medicationRepository.save(medication);

        medicationDto = new MedicationDto("Lu18", 10, "DGSD_18");
        medicationRequest = new MedicationRequest();
        medicationRequest.setCode(medication.getCode());
        medicationRequest.setWeight(medication.getWeight());
        medicationRequest.setName(medication.getName());
        medicationRequest.setId(medication.getId());

        List<MedicationRequest> medicationRequestList = new ArrayList<>();
        medicationRequestList.add(medicationRequest);

        loadDroneRequest = new LoadDroneRequest();
        loadDroneRequest.setDrone(dronePojo);
        loadDroneRequest.setMedicationItems(medicationRequestList);



    }

    @AfterEach
    void tearDown() {
    }


    @Test
    void registerDrone() {
        DroneRegistrationRequest data = new DroneRegistrationRequest();
        data.setModel(ModelType.HEAVY_WEIGHT.getValue());
        data.setState(DroneState.IDLE);
        data.setBatteryCapacity(100);
        data.setWeight(500);
        ResponseEntity<?> responseHelperResponseEntity = droneService.registerDrone(data);
        assertEquals(HttpStatus.OK, responseHelperResponseEntity.getStatusCode());
    }

    @Test
    void testWithNullValueForBattery(){
        DroneRegistrationRequest data = new DroneRegistrationRequest();
        data.setModel(ModelType.LIGHT_WEIGHT.getValue());
        data.setState(DroneState.IDLE);
        data.setBatteryCapacity(null);
        data.setWeight(500);
        assertThrows(CustomException.class, () -> droneService.registerDrone(data));
    }

    @Test
    void testWithNullValueForModel(){
        DroneRegistrationRequest data = new DroneRegistrationRequest();
        data.setModel(" ");
        data.setState(DroneState.IDLE);
        data.setBatteryCapacity(100);
        data.setWeight(500);
        assertThrows(CustomException.class, () -> droneService.registerDrone(data));
    }

    @Test
    void testWithNullValueWieght() throws CustomException {
        DroneRegistrationRequest data = new DroneRegistrationRequest();
        data.setModel(ModelType.MIDDLE_WEIGHT.getValue());
        data.setState(DroneState.IDLE);
        data.setBatteryCapacity(100);
        assertThrows(CustomException.class, () -> droneService.registerDrone(data));
    }


    @Test
    void testLoadItems(){

        ResponseEntity<?> responseHelperResponseEntity = droneService.loadDrone(loadDroneRequest);
        assertEquals(HttpStatus.OK, responseHelperResponseEntity.getStatusCode());

    }

    @Test
    void testCheckLoadedMedicationItems(){
        ResponseEntity<?> responseHelperResponseEntity = droneService.checkLoadedMedicationItems(drone.getId(),DroneState.LOADED);
        assertEquals(HttpStatus.OK, responseHelperResponseEntity.getStatusCode());
    }

    @Test
    void testCheckAvailableDrones(){
        ResponseEntity<?> responseHelperResponseEntity = droneService.checkAvailableDrones(0,10);
        assertEquals(HttpStatus.OK, responseHelperResponseEntity.getStatusCode());
    }

    @Test
    void checkBatteryLevel(){

        ResponseEntity<?> responseHelperResponseEntity = droneService.checkBatteryLevel(drone.getId());
        assertEquals(HttpStatus.OK, responseHelperResponseEntity.getStatusCode());
    }

    @Test
    void getSingleDrone(){
        ResponseEntity<?> responseHelperResponseEntity = droneService.getSingleDrone(drone.getId());
        assertEquals(HttpStatus.OK, responseHelperResponseEntity.getStatusCode());
    }



}
