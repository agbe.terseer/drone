package com.drone.fleetmanagement;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles("test")
@SpringBootTest
class FleetmanagementApplicationTests {

	@Test
	void contextLoads() {
	}

}
